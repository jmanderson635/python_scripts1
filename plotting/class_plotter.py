# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 13:04:53 2020

@author: ande583
"""

from matplotlib.figure import Figure
import numpy as np


class Plotter(object):
    def __init__(self, xval=None,xlabels=None,i=None,data=None,custom=None):
        self.i = i
        self.xval = xval
        self.data = data
        self.xlabels = xlabels
        self.custom = custom
        self.ylist = []
        self.ylabels = []
        self.title_list = []
        y = np.array(self.data[self.i][2])
        self.ylist.append(y)
        y_labels = np.array(self.data[self.i][1])
        self.ylabels.append(y_labels)
        titles = np.array(self.data[self.i][0])
        self.title_list.append(titles)
        print(1,self.i)
        print(2,self.xval)
        print(3,self.data)
        print(4,self.xlabels)
        print(5,self.custom)
        print(6,self.ylist)
        print(7,self.ylabels[0])
        print(8,self.title_list[0])

    def plotgraph(self):
        self.typ = self.custom[0]
        self.linestyle = self.custom[2]
        self.marker = self.custom[3]
        self.color = self.custom[1]
        x = self.xval
        y = self.ylist[0]
        self.linewidth = self.custom[4]
        self.plotapply = self.custom[5]
        title = self.title_list[0]
        ylabel = self.ylabels[0]
        xlabel = self.xlabels
        
        f = Figure(figsize=(2,1),dpi = 100,tight_layout=True)
        ax = f.add_subplot(111)
        f.subplots_adjust(left=.5,bottom=.5,right=.9,top=.9)
        ax.set_ylim(min(y),max(y))
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.title.set_text(title)
        if self.typ == 'Line graph':
            ax.plot(x,y,ls=self.linestyle,c=self.color,lw=self.linewidth)
        if self.typ == 'Scatter Plot':
            ax.scatter(x,y,marker=self.marker,c=self.color)
        if self.typ == 'Bar graph':
            ax.hist(y,bins=50,c=self.color)
        return f
    
    def set_type(self, typ):
        self.typ = typ
    
    def set_linewidth(self, linewidth):
        self.linewidth = linewidth
    
    def set_color(self, color):
        self.color = color
    
    def set_marker(self, marker):
        self.marker = marker
    
    def set_linestyle(self, linestyle):
        self.linestyle = linestyle
    
    def set_apply(self,apply):
        self.plotapply = apply
# =============================================================================
#         if typ == graphs[0]:
#             for i in range(g):
#                 ax = f.add_subplot(g,1,i+1)
#                 ax.clear()
#                 f.subplots_adjust(left=.25,bottom=.1,right=2,top=2)
#                 axs.append(ax)
#                 ax.plot(x,ys[i])
#                 ax.set_ylim(min(ys[i]),max(ys[i]))
#                 if p == 'Apply to all':
#                     ax.clear()
#                     ax.set_xlabel(xlabel)
#                     ax.set_ylabel(ylabels[i])
#                     ax.title.set_text(titles[i])
#                     ax.plot(x,ys[i],ls=ls,c=c,lw=lw) 
#             if p == 'Plot 1': 
#                 axs[0].clear()
#                 axs[0].plot(x,y,ls=ls,c=c,lw=lw)
#                 return f
#             elif p == 'Plot 2':
#                 axs[1].clear()
#                 axs[1].plot(x,y1,ls=ls,c=c,lw=lw)
#                 return f
#             elif p == 'Plot 3':
#                 axs[2].clear()
#                 axs[2].plot(x,y2,ls=ls,c=c,lw=lw)
#                 return f
#             else:
#                 return f
#             
#         if typ == graphs[1]:
#             for i in range(g):
#                 ax = f.add_subplot(g,1,i+1)
#                 ax.clear()
#                 f.subplots_adjust(left=.25,bottom=.1,right=2,top=2)
#                 scatter.append(ax)
#                 ax.scatter(x,ys[i])
#                 ax.set_ylim(min(ys[i]),max(ys[i]))
#                 if p == 'Apply to all':
#                     ax.clear()
#                     ax.set_xlabel(xlabel)
#                     ax.set_ylabel(ylabels[i])
#                     ax.title.set_text(titles[i])
#                     ax.scatter(x,ys[i],marker=m,c=c)      
#             if p == 'Plot 1':
#                 scatter[0].clear()
#                 scatter[0].scatter(x,y,marker=m,c=c)
#                 return f
#             elif p == 'Plot 2':
#                 scatter[1].clear()
#                 scatter[1].scatter(x,y1,marker=m,c=c)
#                 return f
#             elif p == 'Plot 3':
#                 scatter[2].clear()
#                 scatter[2].scatter(x,y2,marker=m,c=c)
#                 return f
#             else:
#                 return f 
#             
#         if typ == graphs[2]:
#             for i in range(g):
#                 ax = f.add_subplot(g,1,i+1)
#                 ax.clear()
#                 f.subplots_adjust(left=.25,bottom=.1,right=2,top=2)
#                 hist.append(ax)
#                 ax.hist(ys[i],bins=50)
#                 if p == 'Apply to all':
#                     ax.hist(ys[i], bins = 50,color=c)
#                     ax.set_xlabel(ylabels[i])
#                     ax.title.set_text(titles[i])   
#             if p == 'Plot 1':
#                 hist[0].hist(y,bins=50,color=c)
#                 return f
#             elif p == 'Plot 2':
#                 hist[1].hist(y1,bins=50,color=c)
#                 return f
#             elif p == 'Plot 3':
#                 hist[2].hist(y2,bins=50,color=c)
#                 return f
#             else:
#                 return f
# =============================================================================
        
        
        
        