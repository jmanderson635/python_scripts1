# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 11:37:08 2020

@author: ande583
"""
import re
import matplotlib.pyplot as plt
import numpy as np
import sys
import argparse
import os
from plt_openfile_function import open_file

my_parcer = argparse.ArgumentParser(description='Creates two scatter plots')

my_parcer.add_argument('-f','--filename',type=str,default='none',help='the data file')

my_parcer.add_argument('-p','--path',type=str,default='.',help='the path to data file')

my_parcer.add_argument('-a','--a',type=int,action='store',default=1,help='the name of a column you want on x-axis')

my_parcer.add_argument('-b','--b',type=int,action='store',default=2,help='the name of a column you want on y-axis')

my_parcer.add_argument('-c','--c',type=int,action='store',default=3,help='the name of the column you want on y-axis of second graph')

args = my_parcer.parse_args()

xaxis = args.a
yaxis = args.b
y1axis = args.c
path = args.path
filename = args.filename
    
f = open(open_file(path,filename),'r')
list = f.readlines()
f.close()

#Parse Row Titles

options = list[0]
for entry in options:
    opt = options.strip()
    opt1 = opt.split('""')
    opt2 = opt.split(',')
    
final_opt = []
for entry in opt2:
    string = entry.lstrip('"').rstrip('"')
    string1 = string.split(' m')
    final_opt.append(string1[0])

final_opt1 = [] 
for entry in final_opt:
    final_string = entry.split(' w')
    final_opt1.append(final_string)

new_list = []
for i in range(len(opt2)):
    new_list.append([i+1,final_opt1[i][0]])
    print(i+1,final_opt1[i][0]) 


if args.a == 1:
    while args.a == 1:
        x = input('Input the number to the left of desired x-axis category.\nIf not changed, first category will be used.\n')
        break
    if x == '':
        xaxis = args.a
    else:
        xaxis = int(x)

if args.b == 2:
    while args.b == 2:
        y = input('Input the number to the left of desired y-axis category.\nIf not changed, second category will be used.\n')
        break
    if y == '':
        yaxis = args.b
    else:
        yaxis = int(y)

if args.c == 3:
    while args.c == 3:
        y1 = input('Input the number to the left of desired y-axis category.\nIf not changed, third category will be used.\n')
        break
    if y1 == '':
        y1axis = args.c
    else:
        y1axis = int(y1)

# Delete first line from the file
del list[0]

# Parse file into usable data

list2 = []
for entry in list:
    s1 = entry.strip()
    s2 = s1.rstrip('\n')
    s3 = s2.split(',')
    list2.append(s3)
    
list3 = []
for i in range(len(list2)):
    for j in range(len(list2[i])):
        str1 = list2[i][j]
        str2 = str1.split()
        list3.append(str2)
        
# Convert values to floats        
list4 = []        
for i in range(len(list3)):
    for j in range(len(list3[i])):
        str3 = list3[i][j]
        list4.append(float(str3))          

# Calculate the column number        

col_num = len(list3[0])
print('Number of columns: ',col_num)

#Take command line arguments and create specified lists
for i in range(len(new_list)):    
    if xaxis == int(new_list[i][0]):
        title = new_list[i][1]
        list_arg = list4[i::col_num]

max_list_arg = max(list_arg)
min_list_arg = min(list_arg)

if abs(min_list_arg) > max_list_arg:
    print('Min value in',title,'column:',min_list_arg)
else:   
    print('Max value in',title,'column:',max_list_arg)    

for i in range(len(new_list)):
    if yaxis == int(new_list[i][0]):
        title1 = new_list[i][1]
        list_arg1 = list4[i::col_num]

max_list_arg1 = max(list_arg1)
min_list_arg1 = min(list_arg1)

if abs(min_list_arg1) > max_list_arg1:
    print('Min value in',title1,'column:',min_list_arg1)
else:   
    print('Max value in',title1,'column:',max_list_arg1)
    
for i in range(len(new_list)):
    if y1axis == int(new_list[i][0]):
        title2 = new_list[i][1]
        list_arg2 = list4[i::col_num]

min_list_arg2 = min(list_arg2)
max_list_arg2 = max(list_arg2)

if abs(min_list_arg2) > max_list_arg2:
    print('Min value in',title2,'column:',min_list_arg2)
else:  
    print('Max value in',title2,'column:',max_list_arg2)

#Plot the specific lists in 2 subplots       

x = np.array(list_arg)
y = np.array(list_arg1)
z = np.array(list_arg2)

fig,ax = plt.subplots(2)
ax[0].scatter(x,y,s=10,marker='.')
ax[0].set(xlabel=title,ylabel=title1)
ax[0].title.set_text(title1)
ax[0].set_xlim(0,max(x))
ax[0].set_ylim(min(y),max(y))
ax[1].scatter(x,z,s=10,marker='.')
ax[1].title.set_text(title2)
ax[1].set_xlim(0,max(x))
if min(z) == 0 and max(z) == 0:
    ax[1].set_ylim(-1,1)
else:
    ax[1].set_ylim(min(z),max(z))
ax[1].set(xlabel=title,ylabel=title2)
plt.tight_layout()
plt.show()





       
