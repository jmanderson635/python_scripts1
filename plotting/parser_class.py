# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 11:22:54 2020

@author: ande583
"""


class Parser(object):
    def __init__(self,lists):
        self.lists = lists
        
    def parse_first(self):
        options = self.lists[0]
    
        for entry in options:
            opt = options.strip()
            opt2 = opt.split(',')
        
        final_opt = []
    
        for entry in opt2:
            string = entry.lstrip('"').rstrip('"')
            final_opt.append(string)
        
                
        return final_opt
    
    def parse_body(self):
        del self.lists[0]

        list2 = []
        for entry in self.lists:
            s1 = entry.strip()
            s2 = s1.rstrip('\n')
            s3 = s2.split(',')
            list2.append(s3)
    
        list3 = []
        for i in range(len(list2)):
            for j in range(len(list2[i])):
                str1 = list2[i][j]
                str2 = str1.split()
                list3.append(str2)
        
# Convert values to floats
    
        list4 = []        
        for i in range(len(list3)):
            for j in range(len(list3[i])):
                str3 = list3[i][j]
                list4.append(float(str3))   
                
        col_num = len(list3[0])
        
        return (col_num,list4)
        
        
        
        
        
        
        
        