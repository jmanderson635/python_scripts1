import argparse
import os
import sys


def open_file(path,filename,mode):
    if path == '.':
        while path == '.':
            paths = input('Would you like to enter an alternate path? (yes/no)\n')
            break
        if paths == 'no' or paths == '':
            path = '.'
        elif paths == 'yes':
            while True:
                paths = input('Input path here or hit enter to use default path.\n')
                if paths == '':
                    path = '.'
                    break
                else:
                    path = paths
                if not os.path.exists(path):
                    print('Path does not exist. Try again.')
                    continue
                break       

    if filename == 'none':
        while True:
            filename = input('Enter the name of the file or enter end to exit:\n')
            if filename == '':
                continue
            if not filename == '':
                full_filename = os.path.join(path,filename)
            if os.path.isfile(full_filename):
                print('File found')
                break
            if filename == 'end':
                sys.exit()   
            if not os.path.isfile(full_filename):
                print('File not found')
                if os.path.exists(path):
                    print('Filename not found')
                    q = input('Enter p to change path or f to change filename\n')
                    if q == 'p':
                        path = input('Enter correct path:\n')
                        print(path)
                        if os.path.exists(path):
                            full_filename = os.path.join(path,filename)
                            print(full_filename)
                        if os.path.isfile(full_filename):
                            print('File found')
                            break
                    elif q == 'f':
                        continue
            if not os.path.exists(path):
                while True:
                    print('Path does not exist')
                    path = input('Re-enter path:\n')
                    full_filename = os.path.join(path,filename)
                    if os.path.isfile(full_filename):
                        break 
                    if not os.path.isfile(full_filename):
                        continue   
               
    full_filename = os.path.join(path,filename)
                  
    if os.path.isfile(full_filename):
        try:
            f = open(full_filename,mode)
            return f
        except OSError as e:
            nf = 'File not found. Cannot open file.'
            print(nf)
            print(e)    
    else:
        while True:
            nf = 'File does not exist. Cannot open file.'
            print(nf)
            if nf == 'File does not exist. Cannot open file.':
                full_filename = input('Enter correct file path with file name to proceed:\n')
                if full_filename == '':
                    continue
                if os.path.isfile(full_filename):
                    f = open(full_filename,mode)
                    return f
                else:
                    continue 
                break










