# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 07:26:38 2020

@author: ande583
"""



from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import tkinter as tk
from tkinter import *
from tkinter import ttk
import numpy as np

from matplotlib.figure import Figure

window = tk.Tk()

window.geometry('400x400+500+100')
window.title('Pflotran Data Viewer')

my_notebook = ttk.Notebook(width=1100,height=900)
my_notebook.pack(side="top", fill="both", expand=True)
myframe1 = ttk.Frame(my_notebook)
myframe1.pack(fill='both',expand=1)
my_notebook.add(myframe1,text='Main')



def frame(val):
    global num
    num = int(val)
    myframes = []
    myframe2 = ttk.Frame(my_notebook)
    myframe2_list = []
    myframe2_list.append(myframe2)
    myframe2.pack(side=tk.TOP,fill='both',expand=1)
    p_frames = []
    for i in range(num):
        st = str(3+i)
        myframe = 'myframe'+st
        myframe = ttk.Frame(myframe2)
        myframe.pack(side=tk.TOP,fill='both',expand=1)
        p_frames.append(myframe)
    myframes.append(myframe2)
    
    for i in range(len(myframes)):
        st = str(i+1)
        tab_title = 'Graph '+st
        my_notebook.add(myframe2,text=tab_title)
    my_notebook.select(myframe2)
    x = (0,1)
    y = (0,1)
    f = Figure(figsize=(2,1),dpi = 100,tight_layout=True)
    plots_list = []
    global canvas_list
    canvas_list = []
    igraph = 1
    for frame in p_frames:
        ax = f.add_subplot(num,1,igraph)
        ax.plot(x,y)
        plots_list.append(ax)
        canvas = FigureCanvasTkAgg(f,window)
        canvas_list.append(canvas)
        canvas.get_tk_widget().pack(side=tk.BOTTOM,fill=tk.BOTH,expand=True)
        igraph += 1
    print(plots_list)
    print(canvas_list)
    plot_button = ttk.Button(myframe2,text='Plot',command=lambda:plot(plots_list))
    plot_button.pack()
#toolbar = NavigationToolbar2Tk(canvas,window)
#toolbar.update()
#canvas._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)

icount = 1
def plot(list_of_plots):
    global icount, canvas_list
    plots_list = list_of_plots
    icount += 1
    for plot in plots_list:
        plot.clear()
        if icount % 2 == 0:           
            x = (1,0)
            y = (0,1)            
        else:            
            x = (1,0)
            y = (1,0)           
        print(x,y)
        plot.plot(x,y)
    for i in range(num):
        canvas_list[i].draw()

global numgraph
numgraph = ttk.Entry(myframe1,width=10)
numgraph.grid(row=1,column=1)
numgraph_label = ttk.Label(myframe1,text='Number of graphs:')
numgraph_label.grid(row=1,column=0)
load = ttk.Button(myframe1,text='Load',command=lambda: frame(numgraph.get()))
load.grid(row=1,column=2)
    

window.mainloop()