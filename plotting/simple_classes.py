# -*- coding: utf-8 -*-
"""
Created on Tue May  4 15:48:35 2021

@author: ande583
"""


from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter.ttk import * 
from gui_openfile_function import open_file
from tkinter import filedialog
from tkinter.filedialog import asksaveasfile
import numpy as np
from list_sort_class import Vars
from clear_class import Clear
from parser_class import Parser
from custom_notebook import CustomNotebook
from matplotlib.figure import Figure
from tkinter.colorchooser import askcolor
from itertools import cycle, islice

                
class XWindow:
    def __init__(self,title,names=None):
        self.names = names
        self.title = title
        self.root = tk.Tk()
        self.root.title(title)
        self.root.geometry('1100x900')
        self.notebook = CustomNotebook(width=1100,height=900)
        self.notebook.pack(side="top", fill="both", expand=True)
        self.f = MainFrame(self.root,self.notebook)
        self.f.pack_frame()
        self.notebook.add(self.f.frame,text='Main')
        self.m = FileMenu(self.root,'File',['New File','Save as','Exit'],[self.new_file, self.save_as, self.closewindow])
        
    def run(self):
        self.root.mainloop()
        
    def save_as(self):
        file = asksaveasfile(filetypes =(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
        
    def new_file(self):
        MainFrame.browse(self.f)
        
    def closewindow(self):
        self.root.destroy()
        
class MainLabel:
    def __init__(self,frame,text):
        self.text = text
        self.tbox1 = ttk.Label(frame,text=self.text)
        self.labels_list = []
        self.labels_list.append(self.tbox1)
        
    def position_label(self,row,col):
        self.tbox1.grid(row=row,column=col)
        
    def change_label(self,text):
        self.text = text
        
class MainButton:
    def __init__(self,frame,text,command=None):
        self.button1 = ttk.Button(frame,text=text,command=command)
        self.button_list = []
        self.button_list.append(self.button1)
        
    def pack_button(self):
        self.button1.pack()
        
    def position_button(self,row,col):
        self.button1.grid(row=row,column=col)
        
    def place_button(self,rely,relx,x,y,anchor):
        self.button1.place(rely=rely,relx=relx,x=x,y=y,anchor=anchor)
          
        
class Variables:
    def __init__(self,text,name,i,var_type):
        self.name = name
        st = str(i)
        self.v = text+st
        self.v = var_type
        self.v.set(self.name[i])
        self.v1 = self.v.get()
        
    def set_variable(self,names):
        self.v.set(names[0])
        
class FileMenu:
    def __init__(self,window,tab_name,func_names,funcs):
        menubar = tk.Menu(window)
        tab = tk.Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = tab_name, menu = tab)
        for i in range(len(func_names)):
            tab.add_command(label = func_names[i], command = funcs[i])
        window.config(menu = menubar)
        
class MainFrame:
    def __init__(self,window,notebook):
        self.notebook = notebook
        self.window = window
        self.frame = ttk.Frame(self.window)
        self.e = ttk.Entry(self.frame,width=50)
        self.e.grid(row=0,column=1,columnspan=3)
        self.e1 = ttk.Entry(self.frame)
        self.load = MainButton(self.frame,'Load', lambda: self.entry_select(self.e1.get()))
        self.b = MainButton(self.frame,'Browse',self.browse)
        self.b.position_button(0,4)
        self.c = MainButton(self.frame,'Close',lambda:XWindow.closewindow(x))
        self.c.place_button(1, 1, 0, 0, SE)
        self.lab = MainLabel(self.frame,'File Path:')
        self.lab.position_label(0,0)
        self.widgets_list = []
        self.drop_down_labels = ['x-axis:','1st y-axis:','2nd y-axis:','3rd y-axis:']
        self.reps = 0
        
    def pack_frame(self):    
        self.frame.pack(fill='both',expand=1)
    
    def browse(self):
        
        self.window.filename = filedialog.askopenfilename(initialdir='.',title='Select A File',filetypes=(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
        if self.window.filename == '':
            self.e.config(state= tk.NORMAL)
            self.e.delete(0,tk.END)
        else:
            self.e.delete(0,tk.END)
            self.e.insert(0,self.window.filename)

    # filename is saved in a variable   
        full_filename = self.window.filename

    # Open the file    
        f = open_file(full_filename,'r')    
        self.list = f.readlines()
        f.close()
    
    # Parse the first line of the data file to find the names of the columns of data
        parser = Parser(self.list)
        self.names = parser.parse_first()
       
        for i in range(len(self.names)):
            self.num = self.names 
        
    # Create the drop down menu to select the number of graphs to be plotted in a frame
        self.e1.grid(row=1,column=1,columnspan=1)
        self.e1_label = MainLabel(self.frame,'Number of graphs:')
        self.e1_label.position_label(1,0)
        self.load.position_button(1,2)
      
    # Initialize the clear class to clear all widgets and labels from the frame when another file is opened
        c = Clear(self.frame,self.widgets_list,self.drop_down_labels,1,0,-1)
        c.clear_all()
        
    def entry_select(self,val):
        self.reps = int(val)
        
        for i in range(int(self.reps)+1):
            if int(self.reps) >= 4:
                 st = str(i+4)
                 l = st+'th y-axis'
                 self.drop_down_labels.append(l)
        
        
        b = self.reps-len(self.names)
        if len(self.names) < self.reps:
            for i in range(b+1):
                self.names.append(self.names[-1])
            
        self.var_names = []
        self.var_objects = []
        self.var_copy = []
        
        for i in range(self.reps+1):
            n = Variables('var',self.names,i,StringVar())
            self.var_names.append(n.v1)
            self.var_objects.append(n.v)
            self.var_copy.append(n.v)
            
    # Revisit CreateDropDowns on Thursday
        drop_down_list = []            
        for i in range(self.reps+1):
            d = CreateDropDowns(self.frame,self.var_objects,self.var_names,self.num,drop_down_list,i,self.names,self.drop_down_labels,self.widgets_list)
        
        z = Clear(self.frame,self.widgets_list,self.drop_down_labels,1,0,self.reps)
        z.clear()
        z.clear_labels()
        p = MainButton(self.frame,'Plot',self.plot)            
        p.position_button(3,4)
        
        
    def plot(self):
        self.plot1 = PlotPage('Plot',self.reps,self.list,self.var_objects,self.var_copy,self.names)
        
class PlotPage:
    def __init__(self,text,reps,list1,var_objects,var_copy,names):
        self.tabframe = ttk.Frame(x.notebook)
        self.tabframe.pack(side=tk.TOP,fill='both',expand=1)
        x.notebook.add(self.tabframe,text=text)
        scroll = XScrollbar(self.tabframe)
        self.p_frames = []
        PlotPage.drop_list = []
        PlotPage.plots = []
        
        for i in range(reps):
            self.myframe = PlotFrame(scroll.scrollbarframe,i,list1,var_objects,var_copy,names,reps)  
            self.p_frames.append(self.myframe)
            
        x.notebook.select(self.tabframe)
            
    def clear_frames(self):             
        for frame in self.p_frames:
            for widget in frame.winfo_children():
                widget.destroy()
            
class PlotFrame:
    def __init__(self,frame,i,list1,var_objects,var_copy,names,reps):
        self.myframe1 = ttk.Frame(frame,width=1100,height=850)
        self.myframe1.pack(side=tk.TOP,fill='both',expand=1)
        self.myframe1.pack_propagate(0)
        canvas1 = PlotCanvas(list1,i,self.myframe1,var_objects,var_copy,names,reps)
                   
class PlotCanvas:
    def __init__(self,list1,i,p_frames,var_objects,var_copy,names,reps):
        self.dats = DataFileOrganizer(p_frames,list1,var_objects,var_copy,names,reps)
        data = self.dats.a.yaxis()
        frames = p_frames
        self.graph = Plotter(frames,self.dats.x,self.dats.x_label,i,data,reps)
        self.graph.plotwidgets()               
                     
class DataFileOrganizer:
    def __init__(self,p_frames,list1,var_objects,var_copy,names,reps):
        
        parse = Parser(list1)
        self.col_num = parse.parse_body()[0]
        list4 = parse.parse_body()[1]
        self.values = np.array(list4)
        
        for i in range(len(var_objects)):
            var_objects[i] = var_copy[i].get()
        
    # Vars is instantiated to sort the data by column according to its column name
        for i in range(reps+1):
            self.a = Vars(var_objects,names,self.values,self.col_num,reps)
        
    # Arrays are created and assigned to variables that will be used in plotting 
        self.x = np.array(self.a.xaxis()[1])
        self.x_label = self.a.xaxis()[0]
            
class XScrollbar:
    def __init__(self,tabframe):
        self.tabframe = tabframe
        self.my_canvas = Canvas(self.tabframe)
        my_vbar=ttk.Scrollbar(self.tabframe, orient = VERTICAL,command=self.my_canvas.yview)
        my_vbar.pack(side= RIGHT, fill= Y)
        my_hbar=ttk.Scrollbar(self.tabframe, orient = HORIZONTAL,command=self.my_canvas.xview)
        my_hbar.pack(side= BOTTOM, fill= X)
        self.scrollbarframe = ttk.Frame(self.my_canvas)
        self.my_canvas.create_window((0,0), window=self.scrollbarframe,anchor="nw", tags="self.tabframe")
        self.my_canvas.configure(yscrollcommand=my_vbar.set)
        self.my_canvas.configure(xscrollcommand=my_hbar.set)
        self.my_canvas.bind('<Configure>',self.on_resize)
        self.my_canvas.pack(side=LEFT,fill=BOTH, expand=1)
        
    def on_resize(self,event):
        width = event.width - 4
        self.my_canvas.itemconfigure('self.tabframe', width=width)
        self.my_canvas.configure(scrollregion=self.my_canvas.bbox("all"))
        

class CreateDropDowns:
    def __init__(self,frame,var_objects,var_names,vals,dd_list,i,names=None,labels=None,widgets_list=None,command=None):
        d = ttk.OptionMenu(frame,var_objects[i],var_names[i],*vals)
        widgets = widgets_list[:] + [d]
        d.grid(row=i+3,column=1)
        d_label = MainLabel(frame,text=labels[i])
        d_label.position_label(i+3,0)
        dd_list.append(d)
        widgets_list.append(widgets)
        d.config(width=len(max(names,key=len)))

class PopupWindow:
    def __init__(self,master,labels,xlabel,ylabel):
        top=self.top=Toplevel(master)
        self.e1=ttk.Entry(top)
        self.e2=ttk.Entry(top)
        if xlabel == labels.get_text():
            self.l= ttk.Label(top,text="Update X-Label")
            self.l.pack()
            lab = ttk.Label(top,text='X-label:')
            lab.pack(side=tk.LEFT)
            self.e1.pack(side=tk.LEFT)
        if ylabel == labels.get_text():
            self.l= ttk.Label(top,text="Update Y-Label")
            self.l.pack()
            lab = ttk.Label(top,text='Y-label:')
            lab.pack(side=tk.LEFT)
            self.e2.pack(side=tk.LEFT)
        self.b=ttk.Button(top,text='Ok',command=self.cleanup)
        self.b.pack()
        
    def cleanup(self):
        self.xlab = self.e1.get()
        self.ylab = self.e2.get()
        self.top.destroy()

class Plotter:
    DEFAULT_LINE_TYPE = 'Line graph'
    DEFAULT_COLOR = 'Blue'
    DEFAULT_MARKER = '.'
    DEFAULT_LINESTYLE = 'solid'
    DEFAULT_LINEWIDTH = 1
    DEFAULT_MARKERSIZE = 10
    def __init__(self,master,xval=None,xlabels=None,i=None,data=None,num_of_plots=None):
        self.master = master
        self.i = i
        self.xval = xval
        self.data = data
        self.num_of_plots = num_of_plots
        self.xlabels = xlabels
        self.ylist = []
        self.ylabels = []
        self.title_list = []
        y = np.array(self.data[self.i][2])
        self.ylist.append(y)
        y_labels = np.array(self.data[self.i][1])
        self.ylabels.append(y_labels)
        titles = np.array(self.data[self.i][0])
        self.title_list.append(titles)
        self.graphtype = self.DEFAULT_LINE_TYPE
        self.linestyle = self.DEFAULT_LINESTYLE
        self.marker = self.DEFAULT_MARKER
        self.markersize = self.DEFAULT_MARKERSIZE
        self.color = self.DEFAULT_COLOR
        self.x = self.xval
        self.y = self.ylist[0]
        self.linewidth = self.DEFAULT_LINEWIDTH
        self.title = self.title_list[0]
        self.ylabel = self.ylabels[0]
        self.xlabel = self.xlabels
        self.f = Figure(figsize=(2,1),dpi = 100,tight_layout=True)
        self.canvas = FigureCanvasTkAgg(self.f,self.master)
        self.ax = self.f.add_subplot(111)
        self.f.subplots_adjust(left=.5,bottom=.5,right=.9,top=.9)
        self.canvas.mpl_connect('pick_event',self.update_labels)
        self.graphtypes = ['Line graph','Scatter plot','Bar graph']
        self.colors = ['Blue','Green','Red','Cyan','Magenta','Yellow','Black','White','Choose a different color']
        colors_cycled = cycle(['Blue','Green','Red','Cyan','Magenta','Yellow','Black'])
        sliced = islice(colors_cycled,None,self.num_of_plots)
        self.result = list(sliced)
        self.markers = ['.',',','o','<','>','v','^','1','2','3','4','8','s','p','P','*','x','X','h','H','+','d','D']
        self.markersizes = [10,50,75,100,150,200,250,300,350,400,450,500]
        self.linestyles = ['solid','dotted','dashed','dashdot',':','None']
        self.linewidths = [1,2,3,4,5,6,7,8,9,10]
        self.custom = [self.graphtypes,self.linestyles,self.linewidths,self.markers,self.markersizes]
        self.first_entry = ['Graph Type','Line Style','Line Width','Marker Type','Marker Size']
        self.commands = [self.set_type,self.set_linestyle,self.set_linewidth,self.set_marker,self.set_markersize]
    
    def plotwidgets(self):
        toolbar = NavigationToolbar2Tk(self.canvas,self.master)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
        
        self.plotoptions_list = []
        self.graphtype1 = StringVar()
        self.color1 = StringVar()
        self.linestyle1 = StringVar()
        self.marker1 = StringVar()
        self.linewidth1 = IntVar()
        self.markersize1 = IntVar()
        self.plotoptions_list.append([self.graphtype1,self.linestyle1,self.linewidth1,self.marker1,self.markersize1])
        
        for j in range(len(self.plotoptions_list[0])):
            rdb = ttk.OptionMenu(self.master,self.plotoptions_list[0][j],self.custom[j][0],*self.custom[j],command=self.commands[j])
            lab = ttk.Label(self.master,text=self.first_entry[j]+' :')
            lab.pack(side=tk.LEFT)
            rdb.pack(side=tk.LEFT)
        self.color = self.result[self.i]
        self.color_menu = ttk.OptionMenu(self.master,self.color1,self.result[self.i],*self.colors,command=self.choose_color)
        color_label = ttk.Label(self.master,text='Color:')
        color_label.pack(side=tk.LEFT)
        self.color_menu.pack(side=tk.LEFT)
        self.plotgraph()
        
    def update_labels(self,event):
        label = event.artist
        mouseevent = event.mouseevent
        self.pop = PopupWindow(self.master,label,self.xlabel,self.ylabel)
        self.master.wait_window(self.pop.top)
        self.entryvalue()
        self.plotgraph()
             
    def entryvalue(self):
        if self.pop.xlab == '' and self.pop.ylab != '':
            self.ylabel = self.pop.ylab 
        if self.pop.ylab == '' and self.pop.xlab != '':
            self.xlabel = self.pop.xlab
        if self.pop.xlab == '' and self.pop.ylab == '':
            pass
        
    def plotgraph(self):
        self.ax.clear()
        self.ax.set_ylim(min(self.y),max(self.y))
        self.ax.set_xlabel(self.xlabel,picker=True)
        self.ax.set_ylabel(self.ylabel,picker=True)
        self.ax.title.set_text(self.title)
        if self.graphtype == 'Line graph':
            self.ax.plot(self.x,self.y,ls=self.linestyle,c=self.color,lw=self.linewidth)
            self.canvas.draw_idle()
        if self.graphtype == 'Scatter plot':
            self.ax.scatter(self.x,self.y,marker=self.marker,s=self.markersize,c=self.color)
            self.canvas.draw_idle()
        if self.graphtype == 'Bar graph':
            self.ax.hist(self.y,bins=50,color=self.color)
            self.canvas.draw_idle()
    
    def set_type(self,*args):
        for entry in self.graphtypes:
            if self.graphtype1.get() == entry:
                self.graphtype = entry
                self.plotgraph()
    
    def set_linewidth(self,*args):
        for entry in self.linewidths:
            if self.linewidth1.get() == entry:
                self.linewidth = entry
                self.plotgraph()
        
    def choose_color(self,*args):
        for entry in self.colors:   
            if self.color1.get() == entry and self.color1.get() != 'Choose a different color':
                self.color = entry
        if self.color1.get() == 'Choose a different color':
            self.color = askcolor(color=self.color)[1]
        self.plotgraph()
        
    def set_marker(self,*args):
        for entry in self.markers:
            if self.marker1.get() == entry:
                self.marker = entry
                self.plotgraph()
                
    def set_markersize(self,*args):
        for entry in self.markersizes:
            if self.markersize1.get() == entry:
                self.markersize = entry
                self.plotgraph()
    
    def set_linestyle(self,*args):
        for entry in self.linestyles:
            if self.linestyle1.get() == entry:
                self.linestyle = entry
                self.plotgraph()
        
if __name__=="__main__":
        
    x = XWindow('Pflotran Data Viewer')
    x.run()
    
    # add user input for each plot option drop down, stats read out, plot axis adjustments
    