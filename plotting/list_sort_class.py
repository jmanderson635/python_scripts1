# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 10:59:27 2020

@author: ande583
"""


class Vars(object):
    def __init__(self,v_list=None,titles=None,data=None,col_num=None,number_of_graphs=None):
        self.v_list = v_list
        self.titles = titles
        self.data = data
        self.col_num = col_num
        self.number_of_graphs = number_of_graphs 
        
    def xaxis(self):
        for i in range(len(self.v_list)):    
            if self.v_list[0] == self.titles[i]:
                list_arg = self.data[i::self.col_num]
                title = self.titles[i]
                return (title,list_arg)
                
    def yaxis(self):
        values = []
        titles = []
        labels = []
        labels_list = []
        final_opt1 = []
        final_list = []
        g = self.number_of_graphs
        v = self.v_list
        
        for entry in v:
            if entry.find('middle'):
                axis_labels = entry.split(' m')
                final_opt1.append(axis_labels)
                labels_list.append(axis_labels)
            else:
                axis_labels = entry.split(' w')
                final_opt1.append(axis_labels)
                labels_list.append(axis_labels)
        
        data = []        
        for i in range(self.col_num):
            values = self.data[i::self.col_num]
            data.append(values)
             
        new_list = []   
        for i in range(g):
            for j in range(self.col_num):
                if v[i+1] == self.titles[j]:
                    list_arg1 = data[j]
                    new_list.append(list_arg1)
            if labels_list[i+1][0] == final_opt1[i+1][0]:
                titles.append(v[i+1])
                labels.append(labels_list[i+1][0])
            final_list.append([titles[i],labels[i],new_list[i]])  
        return final_list
