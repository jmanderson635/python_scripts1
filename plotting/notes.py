# -*- coding: utf-8 -*-
"""
Created on Mon May  3 13:14:21 2021

@author: ande583
"""


from matplotlib import pyplot as plt
from tkinter import ttk
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk


# Make classes for all the attributes of the plots like line width, color, marker, linestyle, etc, as well as each plot, canvas, and frame
# After I make variables and lists of attributes into classes, the next goal is to change the functionality of the plotter. Right now, the 
# plotter is replotting on a new frame generated before the changes from the drop down menus can be implemented. I need to create a class 
# that allows me to take the values from the drop down menus (.get()) and then implement them on the graphs before the regeneration of the plots.

class Xplot:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.typ = "line"
        self.lw = '1'
        self.color = 'blue'
        self.marker = '.'
        self.ls = 'solid'
        
    def set_type(self, typ):
        self.typ = typ
    
    def set_lw(self, lw):
        self.lw = lw
    
    def set_color(self, color):
        self.color = color
    
    def set_marker(self, marker):
        self.marker = marker
    
    def set_ls(self, ls):
        self.ls = ls

class Xwindow:
    def __init__(self,title,notebook):
        self.title = title
        self.root = tk.Tk()
        self.root.title(title)
        self.root.geometry('1100x900+500+100')
        self.notebook = notebook
        
class Xframe:
    def __init__(self):
        self.frame = ttk.Frame(self.notebook)
        
    def add_frame(self, title_of_tab):
        self.notebook.add(self.frame, text=title_of_tab)
        
        
class Xcanvas:
    def __init__(self,figure,frame):
        self.canvas = FigureCanvasTkAgg(self.figure,self.frame)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM,fill=tk.BOTH,expand=True)
        
    def create_toolbar(self):
        self.toolbar = NavigationToolbar2Tk(self.canvas,self.frame)
        self.toolbar.update()
        
    def canvas_pack(self):
        self.canvas._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
        
class Xwidgets:
    def __init__(self,widget_list):
        self.widget_list = widget_list
        
class Labels:
    def __init__(self,text,row_pos,col_pos):
        self.row_pos = row_pos
        self.col_pos = col_pos
        self.text = text
        self.label = ttk.Label(self.frame,text=self.text)
        self.label.grid(row=self.row_pos,column=self.col_pos)
        
class Entry_box:
    def __init__(self,width,row_pos,col_pos,col_span):
        self.row_pos = row_pos
        self.col_pos = col_pos
        self.col_span = col_span
        self.width = width
        self.entry = ttk.Entry(self.frame,self.width)
        self.entry.focus_set()
        self.entry.grid(row=self.row_pos,column=self.col_pos,columnspan=self.col_span)
        
class Dropdowns:
    def __init__(self,var,abrev_name,full_name,r_pos,c_pos):
        self.r_pos = r_pos
        self.c_pos = c_pos
        self.var = var
        self.abrev_name = abrev_name
        self.full_name = full_name
        self.drop_down = ttk.OptionMenu(self.frame,self.var,self.abrev_name,self.full_name)
        self.widgets = self.widget_list[:] + [self.drop_down]
        self.drop_down.grid(row=self.r_pos+3,column=self.c_pos)
        self.widget_list.append(self.widgets)
        
class Button_create:
    def __init__(self,text,command):
        self.text = text
        self.command = command
        self.button = ttk.Button(self.frame,text=self.text,command=self.command)
    














