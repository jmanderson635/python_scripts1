# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 09:54:25 2020

@author: ande583
"""


class Clear(object):
    def __init__(self,window,lists_of_widgets,labels=None,col_widget=None,col_label=None,reps=None):
        self.window = window
        self.lists_of_widgets = lists_of_widgets
        self.labels = labels
        self.col_widget = col_widget
        self.col_label = col_label
        self.reps = reps
    
    def clear(self):
        _list = self.lists_of_widgets
        for item in _list:
            for item in self.window.grid_slaves():
                if int(item.grid_info()['column']) == self.col_widget and int(item.grid_info()["row"]) >= self.reps+4 and int(item.grid_info()["row"]) < len(self.labels):
                    item.destroy()
            return 

    def clear_labels(self):
        for label in self.labels:
            for label in self.window.grid_slaves():
                if int(label.grid_info()["column"]) == self.col_label and int(label.grid_info()["row"]) >= self.reps+4 and int(label.grid_info()["row"]) < len(self.labels):
                    label.grid_forget()
            return
        
    def clear_all(self):
        _list = self.lists_of_widgets
        for item in _list:
            for item in self.window.grid_slaves():
                if int(item.grid_info()['column']) == self.col_widget and int(item.grid_info()["row"]) >= self.reps+3:
                    item.destroy()
                
        for label in self.labels:
            for label in self.window.grid_slaves():
                if int(label.grid_info()["column"]) == self.col_label and int(label.grid_info()["row"]) >= self.reps+3:
                    label.grid_forget()
                