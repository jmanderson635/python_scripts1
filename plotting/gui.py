# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 15:02:06 2020

@author: ande583
"""

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import tkinter as tk
from tkinter import *
from tkinter import ttk
from gui_openfile_function import open_file
from tkinter import filedialog
from tkinter.filedialog import asksaveasfile
from tkinter import colorchooser
import numpy as np
from class_plotter import Plotter
from list_sort_class import Vars
from clear_class import Clear
from parser_class import Parser
from custom_notebook import CustomNotebook

# Create the main window and give it dimensions
window = tk.Tk()

window.geometry('1100x900+500+100')
window.title('Pflotran Data Viewer')

# Create a notebook using the CustomNotebook class and populate it with the Main frame 
my_notebook = CustomNotebook(width=1100,height=900)
my_notebook.pack(side="top", fill="both", expand=True)    
myframe1 = ttk.Frame(my_notebook)
myframe1.pack(fill='both',expand=1)
my_notebook.add(myframe1,text='Main')

# Add a text label and an entry box so the user can input file path
tbox1 = ttk.Label(myframe1,text='File path:')
tbox1.grid(row=0,column=0)

e = ttk.Entry(myframe1,width=50)
e.focus_set()
e.grid(row=0,column=1,columnspan=3)

# Functions that are linked to tabs in menubar cascade
def closewindow():
    window.destroy()
def Save_as():
    file = asksaveasfile(filetypes =(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
def New_File():
    browse()

# Create menubar    
menubar = tk.Menu(window) 
   
# Adding File Menu and commands           
tab = tk.Menu(menubar, tearoff = 0) 
menubar.add_cascade(label = 'File', menu = tab)
tab.add_command(label = 'New File', command = New_File)
tab.add_command(label = 'Save as', command = Save_as)  
tab.add_command(label = 'Exit', command = closewindow)

# Display Menu 
window.config(menu = menubar) 

widgets_list = []
drop_down_labels = ['x-axis:','1st y-axis:','2nd y-axis:','3rd y-axis:']

# The browse function is connected to a button that launches a file dialog which allows the user to select a file 
def browse():
    window.filename = filedialog.askopenfilename(initialdir='.',title='Select A File',filetypes=(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
    if window.filename == '':
        e.config(state= tk.NORMAL)
        e.delete(0,tk.END)
    else:
        e.delete(0,tk.END)
        e.insert(0,window.filename)
        
    # filename is saved in a varibale     
    full_filename = e.get()
    
    # Open the file    
    f = open_file(full_filename,'r')
    global list
    list = f.readlines()
    f.close()
    
    global final_opt
    
    #Parse the first line of the data file to find the names of the columns of data
    parser = Parser(list)
    final_opt = parser.parse_first()
       
    global num
    
    for i in range(len(final_opt)):
        num = final_opt
    
    # Create the drop down menu to select the number of graphs to be plotted in a frame
    global numgraph
    numgraph = ttk.Entry(myframe1,width=10)
    numgraph.grid(row=1,column=1)
    numgraph_label = ttk.Label(myframe1,text='Number of graphs:')
    numgraph_label.grid(row=1,column=0)
    load.grid(row=1,column=2)
      
    # Initialize the clear class to clear all widgets and labels from the frame when another file is opened
    c = Clear(myframe1,widgets_list,drop_down_labels,1,0,-1)
    c.clear_all()
    
def entry_select(val):
    global q
    q = int(val)
    for i in range(int(numgraph.get())+1):
        nums = int(numgraph.get())
        if nums >= 4:
            st = str(i+4)
            l = st+'th y-axis'
            drop_down_labels.append(l)
            
    global v_list
    global var_list
    global var_copy
    var_list = []
    var_copy = []
    v_list = []
    
    b = q-len(final_opt)
    if len(final_opt) < q:
        for i in range(b+1):
            final_opt.append(final_opt[-1])
    
    for i in range(q+1):
        st = str(i)
        v = 'var'+st
        v = StringVar()
        v.set(final_opt[i])
        v1 = v.get()
        v_list.append(v1)
        var_list.append(v)
        var_copy.append(v)
   
    width = len(max(final_opt, key=len))
    for i in range(q+1):
        drop_down = ttk.OptionMenu(myframe1,var_list[i],v_list[i],*num)
        widgets = widgets_list[:] + [drop_down]
        drop_down.grid(row=i+3,column=1)
        drop_down_label = ttk.Label(myframe1,text=drop_down_labels[i])
        drop_down_label.grid(row=i+3,column=0) 
        widgets_list.append(widgets)
        drop_down.config(width=width)    
    b = Clear(myframe1,widgets_list,drop_down_labels,1,0,q)
    b.clear()
    b.clear_labels()            
    plotting.grid(row=3,column=4)
    
# Drop down menus will be created based on the number of graphs selected     
myframes = []

# Numbers are sorted then plotted according to the specifications given by the user  
def plot(event=None):
    # Parser class is instantiated to sort the body of the data file into usable, plottable data
    parse = Parser(list)
    col_num = parse.parse_body()[0]
    list4 = parse.parse_body()[1]
    values = np.array(list4)
    
    for i in range(len(var_list)):
        var_list[i] = var_copy[i].get()
    
    # Vars is instantiated to sort the data by column according to its column name
    for i in range(q+1):
        a = Vars(var_list,final_opt,values,col_num,q)
    
    # Arrays are created and assigned to variables that will be used in plotting          
    x = np.array(a.xaxis()[1])
    x_label = a.xaxis()[0]
    
    # Lists that will populate the drop down menus for user customization
    labels = ['Line graph','Scatter plot','Bar graph']
    colors = ['Blue','Green','Red','Cyan','Magenta','Yellow','Black','White']
    marker = ['.',',','o','<','>','v','^','1','2','3','4','8','s','p','P','*','x','X','h','H','+','d','D']
    linestyles = ['solid','dotted','dashed','dashdot',':','None']
    linewidth = [1,2,3,4,5,6,7,8,9,10]
    plot_typ = ['Apply to all','Plot 1','Plot 2','Plot 3']
    custom = [labels,colors,marker,linestyles,linewidth,plot_typ]
    first_entry = ['Graph Type','Color','Marker Type','Line Style','Line Width','Apply Changes']
    
    # Variables are created for new drop down menus that will allow the user to customize the plot
    custom_list = []
    cust = []
    cust_copy = []
    for i in range(q):
        st = str(i)
        r = 'r'+st
        c = 'c'+st
        line = 'l'+st
        m = 'm'+st
        lw = 'lw'+st
        p = 'p'+st
        r = StringVar()
        c = StringVar()
        line = StringVar()
        m = StringVar()
        lw = IntVar()
        p = StringVar()
        r.set(labels[i])
        c.set(colors[i])
        line.set(linestyles[i])
        m.set(marker[i])
        lw.set(linewidth[i])
        p.set(plot_typ[i])
        custom_list.append([r,c,line,m,lw,p])
        cust.append([r,c,line,m,lw,p])
        cust_copy.append([r,c,line,m,lw,p])
    print(custom_list)
    print(cust)
    print(cust_copy)
    
    # Create a new frame every time plotting occurs
    
    myframe2 = ttk.Frame(my_notebook)
    myframe2_list = []
    myframe2_list.append(myframe2)
    myframe2.pack(side=tk.TOP,fill='both',expand=1)
    p_frames = []
    for i in range(q):
        st = str(3+i)
        myframe = 'myframe'+st
        myframe = ttk.Frame(myframe2)
        myframe.pack(side=tk.TOP,fill='both',expand=1)
        p_frames.append(myframe)
    myframes.append(myframe2)
            
        # The name of the frame 
    for i in range(len(myframes)):
        st = str(i+1)
        tab_title = 'Graph '+st
        my_notebook.add(myframe2,text=tab_title)
               
    plots = p_frames
    for frame in p_frames:
        for widget in frame.winfo_children():
            widget.pack_forget()
             
    for i in range(q):
        for j in range(6):
            cust[i][j] = cust_copy[i][j].get()
    print(cust)
    data = a.yaxis()
    custom_styles = cust
    frames = plots
    ylist = []
    ylabels = []
    title_list = []
    for i in range(q):
        y = np.array(data[i][2])
        ylist.append(y)
        y_labels = np.array(data[i][1])
        ylabels.append(y_labels)
        titles = np.array(data[i][0])
        title_list.append(titles)
        graph = Plotter(x,ylist[i],title_list[i],ylabels[i],x_label,custom_styles[0])                
        canvas = FigureCanvasTkAgg(graph.plotgraph(),frames[i])
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM,fill=tk.BOTH,expand=True)
        toolbar = NavigationToolbar2Tk(canvas,frames[i])
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
        
    # Drop down menus are created for user selection of graph customization
    # Open color selection window    
    def color():
        print(color_list)
        color = colorchooser.askcolor()
        for i in range(len(color_list)):
            if color_list[i] in p_frames[i].winfo_children():
                custom_list[i][1].set(color[1])
            
    # Button activates color selection window
    color_list = []
    for i in range(len(plots)):
        color_btn = ttk.Button(plots[i],text='Choose a different color',command= color)
        color_list.append(color_btn)
        color_btn.pack(side=tk.RIGHT)
        for j in range(len(custom)):
            rdb = ttk.OptionMenu(plots[i],custom_list[i][j],custom[j][0],*custom[j],command=plot)
            lab = ttk.Label(plots[i],text=first_entry[j]+' :')
            lab.pack(side=tk.LEFT)
            rdb.pack(side=tk.LEFT)
            
    my_notebook.select(myframe2) 
               
# Buttons are created 
load = ttk.Button(myframe1,text='Load',command=lambda: entry_select(numgraph.get()))
file_browse = ttk.Button(myframe1,text='Find file',command=browse)  
close = ttk.Button(window,text='Close',command=closewindow)
plotting = ttk.Button(myframe1,text='Plot',command= plot)

# Buttons are added to the frame 
file_browse.grid(row=0,column=4)
close.place(rely=1.0, relx=1.0, x=0, y=0, anchor=SE)


window.mainloop()






