# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 08:25:58 2020

@author: ande583
"""


from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import tkinter as tk
from tkinter import *
from tkinter import ttk
import numpy as np
from matplotlib.figure import Figure
from gui_openfile_function import open_file
from parser_class import Parser


class MainWindow:
    def __init__(self,title):
        
        self.root = tk.Tk()
        self.root.title(title)
        
        def browse():
            self.filename = filedialog.askopenfilename(initialdir='.',title='Select A File',filetypes=(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
            if self.filename == '':
                self.e.config(state= tk.NORMAL)
                self.e.delete(0,tk.END)
            else:
                self.e.delete(0,tk.END)
                self.e.insert(0,self.filename)
                
                # filename is saved in a variable    
                full_filename = self.e.get()
        
            # Open the file    
            f = open_file(full_filename,'r')
            list = f.readlines()
            f.close()
            
            parser = Parser(list)
            final_opt = parser.parse_first()
            
            numgraph = ttk.Entry(self.root,width=10)
            numgraph.grid(row=1,column=1)
            numgraph_label = ttk.Label(self.root,text='Number of graphs:')
            numgraph_label.grid(row=1,column=0)
            self.load.grid(row=1,column=2)
            
        def closewindow():
            self.root.destroy()
            
        #def entry_select(val):
            
            
        
        self.notebook = ttk.Notebook(self.root)
        
        self.tbox1 = ttk.Label(self.root,text='File path:')
        self.tbox1.grid(row=0,column=0)
        
        self.e = ttk.Entry(self.root,width=50)
        self.e.focus_set()
        self.e.grid(row=0,column=1,columnspan=3)
        
        self.load = ttk.Button(self.root,text='Load',command= lambda:entry_select(numgraph.get()))
        self.file_browse = ttk.Button(self.root,text='Find file',command=browse)  
        self.close = ttk.Button(self.root,text='Close',command=closewindow)
        self.plotting = ttk.Button(self.root,text='Plot')
        
        # Buttons are added to the frame 
        self.file_browse.grid(row=0,column=4)
        self.close.grid(column=99,row=99)
        
    

    def add_tab(self,title,text):
        frame = ttk.Frame(self.notebook)
        self.notebook.add(frame,text=title)
        label = ttk.Label(frame,text=text)
        label.grid(column=1,row=1)
        self.notebook.grid()
        
       
    def run(self):
        self.root.mainloop()
      
# Create a plot class that owns the canvas and the frame - In order to add them to the tabs
# Create a database with all plot information and use the database as the input for the plot class  
# Customization Options

class Plot:

    
    def __init__(self,x,y,title):
        self.x = x
        self.y = y
        self.title = title
        self.f = Figure(figsize=(2,1),dpi = 100,tight_layout=True)
        self.ax = self.f.add_subplot(111)
        self.f.subplots_adjust(left=.5,bottom=.5,right=.9,top=.9)
        self.canvas = FigureCanvasTkAgg(self.f)
        frames = MainWindow().add_tab()
        self.frame1 = ttk.Frame(frames)       
        
    def set_labels(self,xlabel,ylabel):        
        self.ax.set_ylim(min(self.y),max(self.y))
        self.ax.set_xlim(min(self.x),max(self.x))
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)
        self.ax.title.set_text(self.title)
        
        
class Dropdown:
    
    def __init__(self, dropdown_list_items):
        self.dropdown = ttk.OptionMenu(self.myframe1)
        self.dropdown.pack()
        
    def custom_dropdown(self):
        self.dropdown = ttk.OptionMenu(self.frame1)
        self.dropdown.pack()
        



        
if __name__=="__main__":
    nb = MainWindow('Pflotran Data Viewer')
    nb.run()
    

        
        