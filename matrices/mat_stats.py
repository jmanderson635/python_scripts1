# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 16:04:13 2020

@author: ande583
"""


import sys
import re
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
from mat_openfile_function import open_file

parser = argparse.ArgumentParser(description='Compute matrix statistics')

parser.add_argument('-p','--path',type=str,default='.',help='the path to data file')

parser.add_argument('-f','--filename',default='none',type=str,help='the filename of the data')

args = parser.parse_args()

filename = args.filename
path = args.path

f = open(open_file(path,filename),'r')
list = f.readlines()
f.close()
    
   
# Delete the first two lines from the file
del list[0:2]

# Cut existing strings and assign to a new list1
list1 = []
for entry in list:
    s1 = entry.strip()
    s2 = re.sub(r'.*:\s*','',s1).lstrip('(').rstrip(')').split(')  (')
    list1.append(s2)
    
# Find number of rows   
row_num = len(list1)
print('Number of rows:',row_num)

# Find the column number for each row and assign it to list_std
# Add the number of columns per row to find the total number of entries
list_std = []
i = 0
sum1 = 0
while i < len(list1):
    sub_list = list1[i]
    col_num = len(sub_list)
    list_std.append(col_num)
    #print('Number of columns:',col_num)
    sum1 = sum1 + col_num
    i += 1
print('Total Number of Entries:',sum1)

# Calculate the average number of entries per row
aver = sum1/row_num
print('Average number of entries per row:',aver)

# Calculate the standard deviation of the list of column numbers
st_dev = np.std(list_std)
print('Standard Deviation of the number of entries per row:',st_dev)

# Cut existing strings and convert to integers and floats
irow = 0
list2 = []
list3 = []
list4 = []
for i in range(len(list1)):
    new_row = []
    for j in range(len(list1[i])):
        l = list1[i][j]
        l2 = l.split(',')
        list3.append(i)
        list4.append(float(l2[1]))
        new_row.append([i,int(l2[0]),float(l2[1])])
    irow += 1
    list2.append(new_row)
    
# Find the number of non-zero and zero values in list2
count1 = 0
for i in range(len(list2)):
    for j in range(len(list2[i])):
        if list2[i][j][2] > 0 or list2[i][j][2] < 0:
            count1 += 1
print('Number of non-zero entries:',count1)

count2 = 0
for i in range(len(list2)):
    for j in range(len(list2[i])):
        if list2[i][j][2] == 0:
            count2 += 1
print('Number of zero entries:',count2)

# Find the maximum and minimum number of columns per row
max1 = 0
min1 = 1.e20
i = 0
while i < len(list1):
    sub_list2 = list1[i]
    col_num = len(sub_list2)
    if col_num > max1:
        max1 = col_num
    elif col_num < min1:
        min1 = col_num
    i += 1
print('Maximum number of entries:',max1)
print('Minimum number of entries:',min1)
       
# Find row, column, and value of the maximum absolute value using list4
max_abs = -1.e20
for row in list2:
    icol = 0
    for entry in row:
        if abs(entry[2]) > max_abs:
            max_abs = abs(entry[-1])
            col_maxinrow = icol
            col_max = entry[1]
            row_max = entry[0]
        icol += 1
    
print('Maximum value {} at row {} column {}'.
    format(list2[row_max][col_maxinrow],row_max,col_max))
print('Maximum Absolute Value:',max_abs)

# Convert lists of rows and values to arrays
x = np.array(list3)
y = np.array(list4)

# Use subplots to plot a scatter plot and histogram of the values
fig,ax = plt.subplots(2)
ax[0].scatter(x,y,s=.25,marker='.')
ax[0].set_xlim(0,row_num)
ax[0].set_ylim(-.5,.5)
ax[0].title.set_text('Scatter Plot')
ax[0].set(xlabel='Row Number',ylabel='Values')
ax[1].hist(y,bins=50,range=[min(y),max(y)],width=.025,color='blue')
ax[1].title.set_text('Histogram')
ax[1].set(xlabel='Values',ylabel='Number of Entries')
plt.tight_layout()
plt.show()


    

          
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
